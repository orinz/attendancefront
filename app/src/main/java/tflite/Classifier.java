package tflite;

/* Copyright 2019 The TensorFlow Authors. All Rights Reserved.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
==============================================================================*/

import android.graphics.Bitmap;
import android.graphics.RectF;

import java.util.List;

/**
 * Generic interface for interacting with different recognition engines.
 */

/**
 * Generic interface for interacting with different recognition engines.
 */
public interface Classifier {
    Recognition recognizeImage(Bitmap bitmap);
    /**
     * An immutable result returned by a Classifier describing what was recognized.
     */
    public class Recognition {

        private final String title;
        private final Float confidence;
        private int detectedClass;
        public Recognition(
               final String title, final Float confidence, int id) {
            this.title = title;
            this.confidence = confidence;
            this.detectedClass = id;
        }
        public String getTitle() {
            return title;
        }
        public Float getConfidence() {
            return confidence;
        }
        public int getDetectedClass() {
            return detectedClass;
        }

    }
}