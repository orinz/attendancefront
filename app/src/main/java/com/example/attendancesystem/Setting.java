package com.example.attendancesystem;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

import com.example.remote.ApiUtils;

public class Setting extends AppCompatActivity {
Button btnSave;
EditText edtLinkServer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.setting);

        //
        btnSave = (Button) findViewById(R.id.btnSave);
        edtLinkServer = (EditText) findViewById(R.id.edtServerLink);
        getSharePref();

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ApiUtils.BASE_URL = edtLinkServer.getText().toString();
                setSharePref();
                Intent returnIntent = new Intent();
                returnIntent.putExtra("result",true);
                setResult(MainActivity.RESULT_OK, returnIntent);
                finish();
            }
        });
    }
    void getSharePref(){
        SharedPreferences pref = getApplicationContext().getSharedPreferences("AttendancePref", MODE_PRIVATE);
        ApiUtils.BASE_URL = pref.getString("serverLink", ApiUtils.BASE_URL);
        edtLinkServer.setText(ApiUtils.BASE_URL);
    }

    void setSharePref(){
        SharedPreferences pref = getApplicationContext().getSharedPreferences("AttendancePref", MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("serverLink", ApiUtils.BASE_URL);
        editor.apply(); // commit changes
    }
}