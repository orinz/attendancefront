package com.example.attendancesystem;

import android.content.ContentValues;
import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;


import android.graphics.Bitmap;
import android.provider.MediaStore;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.RetinaFace.RetinaFace;
import com.example.remote.ApiUtils;
import com.example.remote.UserService;
import com.example.service.ResAttend;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import tflite.Classifier;
import tflite.TFLiteObjectDetectionAPIModel;


public class AttendanceActivity extends AppCompatActivity {
    private static final String TF_OD_API_MODEL_FILE1 = "210927_cnn_invert_v2.tflite";
    private static final String TF_OD_API_MODEL_FILE = "20211209_V2.tflite";
    public static final int TF_OD_API_INPUT_SIZE = 256;
    private static final boolean TF_OD_API_IS_QUANTIZED = false;
    private static final boolean MAINTAIN_ASPECT = true; // ==true: crop area in center

    Button cameraButton;
    ImageView imgFace;
    TextView tvInfor;
    private Classifier clsSpoof;

    private final static int REQUEST_IMAGE_CAPTURE = 124;
    Uri tempFileImage;
    private RetinaFace retinaface = new RetinaFace();
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.result_face);
        AssetManager asm = getAssets();
        //face retina init
        boolean ret_init = retinaface.Init(getAssets());
        //face spoofing init
        try {
            clsSpoof = TFLiteObjectDetectionAPIModel.create(
                    getAssets(),
                    TF_OD_API_MODEL_FILE,
                    TF_OD_API_INPUT_SIZE,
                    TF_OD_API_IS_QUANTIZED);
        } catch (IOException e) {
            e.printStackTrace();
            Log.e("Attendance", "Exception initializing classifier spoof!");
            finish();
        }

        // finding the elements by their id's alloted.
        cameraButton = findViewById(R.id.btnTake);
        imgFace = findViewById(R.id.imgFace);
        tvInfor = findViewById(R.id.tvInfor);
        // to request image capture using camera
        cameraButton.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final ContentValues contentValues = new ContentValues();
//                        contentValues.put(MediaStore.MediaColumns.DISPLAY_NAME, "my_photo.jpg");

                        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd_HHmmss");
                        Date curDate = new Date(System.currentTimeMillis());
                        String str = formatter.format(curDate);
                        contentValues.put(MediaStore.MediaColumns.DISPLAY_NAME, str + ".jpg");

                        contentValues.put(MediaStore.MediaColumns.MIME_TYPE, "image/jpg");
                        contentValues.put(MediaStore.MediaColumns.RELATIVE_PATH, Environment.DIRECTORY_DCIM);

                        final Uri contentUri = getBaseContext().getContentResolver().insert(
                                MediaStore.Images.Media.EXTERNAL_CONTENT_URI, contentValues);

                        final Intent captureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        captureIntent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                        captureIntent.putExtra(MediaStore.EXTRA_OUTPUT, contentUri);
                        tempFileImage = contentUri;
                        startActivityForResult(captureIntent, REQUEST_IMAGE_CAPTURE);
                    }
                });
    }

    @Override
    protected void onActivityResult(int requestCode,
                                    int resultCode,
                                    Intent data)
    {
        // after the image is captured, ML Kit provides an
        // easy way to detect faces from variety of image
        // types like Bitmap
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            Bitmap bitmap = null;
            try {
                //bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), tempFileImage);
                bitmap = decodeUri((tempFileImage));
                detectFace(bitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    private Bitmap decodeUri(Uri selectedImage) throws FileNotFoundException {
        // Decode image size
        BitmapFactory.Options o = new BitmapFactory.Options();
        o.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(getContentResolver().openInputStream(selectedImage), null, o);

        // The new size we want to scale to
        final int REQUIRED_SIZE = 400;

        // Find the correct scale value. It should be the power of 2.
        int width_tmp = o.outWidth, height_tmp = o.outHeight;
        int scale = 1;
        while (true) {
            if (width_tmp / 2 < REQUIRED_SIZE
                    || height_tmp / 2 < REQUIRED_SIZE) {
                break;
            }
            width_tmp /= 2;
            height_tmp /= 2;
            scale *= 2;
        }

        // Decode with inSampleSize
        BitmapFactory.Options o2 = new BitmapFactory.Options();
        o2.inSampleSize = scale;
        return BitmapFactory.decodeStream(getContentResolver().openInputStream(selectedImage), null, o2);
    }
    
    void detectFace(final Bitmap bitmap) throws IOException {
        int angle = 0;
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        Bitmap scaledBitmap = Bitmap.createScaledBitmap(bitmap, bitmap.getWidth(), bitmap.getHeight(), true);
        Bitmap rotatedBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0, scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix, true);
        imgFace.setImageBitmap(rotatedBitmap);

        //retina
        float[] face_info = retinaface.Detect(rotatedBitmap);//x,y,w,h
        System.out.println(Arrays.toString(face_info));
        if( face_info != null){
            int w = (int) (face_info[2] - face_info[0]);
            int h = (int) (face_info[3] - face_info[1]);

//            int degree = (int) Math.toDegrees(Math.atan2((face_info[7] - face_info[5]), (face_info[6] - face_info[4])));
//
//            Matrix mt = new Matrix();
//            mt.postRotate(degree);
//            Bitmap subFace1 = Bitmap.createBitmap(rotatedBitmap, 0, 0, scaledBitmap.getWidth(), scaledBitmap.getHeight(), mt, true);
//            System.out.println(degree);
            Bitmap subFace = Bitmap.createBitmap(rotatedBitmap, (int) face_info[0], (int) face_info[1], w, h);
            imgFace.setImageBitmap(subFace);
            if(checkSpoof(subFace)){
                tvInfor.setText(getString(R.string.face_detected)+ " LÀ GIẢ MẠO");
                Toast.makeText(AttendanceActivity.this, "Bạn cần thực hiện chụp lại ảnh thật để được điểm danh!", Toast.LENGTH_SHORT).show();
            }
            else {
                tvInfor.setText(getString(R.string.face_detected)+ " ĐƯỢC CHẤP NHẬN");
                Intent intent = getIntent();
                Integer passCode = intent.getIntExtra("passCode", 0);
                String token_user = intent.getStringExtra("token_user");
                Integer IDClass = intent.getIntExtra("IDClass", 0);

                Log.d("gao.tv", "nhan ve "+passCode+token_user+IDClass);


                //save subFace
                String root = Environment.getExternalStorageDirectory().toString();
                File myDir = new File(root + "/saved_images");
                myDir.mkdirs();
                File file = new File(myDir, "subFace.jpg");
                if (file.exists()) file.delete();

                try {
                    FileOutputStream out = new FileOutputStream(file);
                    subFace.compress(Bitmap.CompressFormat.JPEG, 100, out);
                    out.flush();
                    out.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }

                Log.d("gao.tv", file.toString());
                UserService userService = ApiUtils.getUserService(ApiUtils.BASE_URL);

                RequestBody requestFile = RequestBody.create(
                        MediaType.parse("multipart/form-data"),file);
                MultipartBody.Part fileBody =
                        MultipartBody.Part.createFormData("file",
                                file.getName(),
                                requestFile);

                Call call = userService.search("search", token_user, passCode, IDClass+"", fileBody);
                call.enqueue(new Callback() {
                    @Override
                    public void onResponse(Call call, Response response) {
                        Log.d("gao.tv", response.message()+response.isSuccessful());
                        if(response.isSuccessful()){
                            ResAttend resAttend = (ResAttend) response.body();
                            if(resAttend.status.equals(200)){
                                if(resAttend.message.equals("Điểm danh thành công")){
                                    Toast.makeText(AttendanceActivity.this, "Điểm danh thành công!", Toast.LENGTH_SHORT).show();
                                    Log.d("gao.tv", "Điểm danh thành công!");
                                }
                                else if(resAttend.message.equals("Đã điểm danh")){
                                    Toast.makeText(AttendanceActivity.this, "Bạn đã thực hiện điểm danh rồi!", Toast.LENGTH_SHORT).show();
                                    Log.d("gao.tv", "Bạn đã thực hiện điểm danh rồi!");
                                }
                                else if(resAttend.message.equals("Điểm danh thất bại")){
                                    Toast.makeText(AttendanceActivity.this, "Điểm danh thất bại!", Toast.LENGTH_SHORT).show();
                                    Log.d("gao.tv", "Điểm danh thất bại!");
                                }

                            }else{
                                Log.d("gao.tv", "loi roai!!");
                            }
                        }
                        else{
                            Log.d("gao.tv", "Response Not Success");
                        }
                    }
                    @Override
                    public void onFailure(Call call, Throwable t) {
                        Log.d("gao.tv", "loi roai!");
                    }
                });

            }
        }
        else{
            Bitmap center = Bitmap.createBitmap(rotatedBitmap, (int) (rotatedBitmap.getWidth()/2-128), (int)(rotatedBitmap.getHeight()/2-128) , 256, 256);

            if(checkSpoof(center))
                tvInfor.setText("No Face is spoof");
            else
                tvInfor.setText("No Face is live");
            imgFace.setImageBitmap(center);
            //imgFace.setImageResource(0);
            //tvInfor.setText("sss");
        }

        return;
    }

    boolean checkSpoof(Bitmap face){
        Bitmap croppedBitmap = Bitmap.createBitmap(TF_OD_API_INPUT_SIZE, TF_OD_API_INPUT_SIZE, Bitmap.Config.ARGB_8888);
        Matrix frameToCropTransform =  getTransformationMatrix(face.getWidth(), face.getHeight(), TF_OD_API_INPUT_SIZE, TF_OD_API_INPUT_SIZE, MAINTAIN_ASPECT);
        final Canvas canvas = new Canvas(croppedBitmap);
        canvas.drawBitmap(face, frameToCropTransform, null);
        imgFace.setImageBitmap(croppedBitmap);
        Classifier.Recognition res =  clsSpoof.recognizeImage(croppedBitmap);

        Log.d("checkSpoof", res.getTitle() + " id:"+ res.getDetectedClass()+ "  conf:" + res.getConfidence());
        if(res.getDetectedClass() == 0)
            return false;
        return true;
    }
    public static Matrix getTransformationMatrix(
            final int srcWidth,
            final int srcHeight,
            final int dstWidth,
            final int dstHeight,
            final boolean maintainAspectRatio) {
        final Matrix matrix = new Matrix();
        // Apply scaling if necessary.
        if (srcWidth != dstWidth || srcHeight != dstHeight) {
            final float scaleFactorX = dstWidth / (float) srcWidth;
            final float scaleFactorY = dstHeight / (float) srcHeight;
            if (maintainAspectRatio) {
                // Scale by minimum factor so that dst is filled completely while
                // maintaining the aspect ratio. Some image may fall off the edge.
                final float scaleFactor = Math.min(scaleFactorX, scaleFactorY);
                matrix.postScale(scaleFactor, scaleFactor);
            } else {
                // Scale exactly to fill dst from src.
                matrix.postScale(scaleFactorX, scaleFactorY);
            }
        }

        return matrix;
    }
}
