package com.example.attendancesystem;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;

import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.model.Classroom;
import com.example.remote.ApiUtils;
import com.example.remote.UserService;
import com.example.service.ResListClass;
import com.example.service.ResLogout;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ClassListActivity extends AppCompatActivity {

    public static int[] imgAvatar = {R.drawable.logo_class, R.drawable.logo_class, R.drawable.logo_class, R.drawable.logo_class, R.drawable.logo_class, R.drawable.logo_class, R.drawable.logo_class, R.drawable.logo_class, R.drawable.logo_class, R.drawable.logo_class};
    public static String[] tvNoiDung = {"Computer Vision", "Machine Learning",  "Processing Image"};
    public static String[] tvStatus = {"0", "0",  "0"};
    public static String[] LstIDClass;
    public static String[] LstStatus;

    TextView txtUser;
    ListView lvCustomListView;
    Button btnLogout;
    UserService userService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_class);

        txtUser = (TextView) findViewById(R.id.user_listclass);
        btnLogout = (Button) findViewById(R.id.logout_button);
        userService = ApiUtils.getUserService(ApiUtils.BASE_URL);

        Intent intent = getIntent();
        String token_user = intent.getStringExtra("token_user");//if it's a string you stored.
        Integer passCode = intent.getIntExtra("passCode", 0);
        Integer permission = intent.getIntExtra("permission", 0);

        txtUser.setText("CLASS LIST OF " + passCode);
        //get ListView theo ID từ layout xml
        lvCustomListView = (ListView) findViewById(R.id.lvClass);
        ClassListAdapter lvAdapter = new ClassListAdapter(ClassListActivity.this, tvNoiDung, tvStatus, imgAvatar, permission,passCode, token_user);
        lvCustomListView.setAdapter(lvAdapter);

        //get listclass
        Call call = userService.listclass("listclass", passCode, token_user);

        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
//                Log.d("gao.tv", "List class "+response.isSuccessful());

                if(response.isSuccessful()){
                    ResListClass resListClass = (ResListClass) response.body();
                    Classroom classroom = resListClass.info;

                    if(resListClass.status.equals(200)){
                        LstIDClass = new String[classroom.IDClass.length];
                        LstStatus = new String[classroom.IDClass.length];
                        for(int i=0; i<classroom.IDClass.length; i++){
                            LstIDClass[i] = classroom.IDClass[i]+"";
                            LstStatus[i] = classroom.Status[i]+"";
                        }

//                        ClassListAdapter lvAdapter = new ClassListAdapter(ClassListActivity.this, LstIDClass, imgAvatar, permission);
//                        lvCustomListView.setAdapter(lvAdapter);
                        lvAdapter.result = LstIDClass;
                        lvAdapter.status = LstStatus;

                        lvAdapter.notifyDataSetChanged();
                    }else{
                        Toast.makeText(ClassListActivity.this, "The xxx is incorrect!", Toast.LENGTH_SHORT).show();
                    }
                }else{
                    Toast.makeText(ClassListActivity.this, "The xxx is incorrect!", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                Toast.makeText(ClassListActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });



        btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Call call = userService.logout("logout", passCode, token_user);
                call.enqueue(new Callback() {
                    @Override
                    public void onResponse(Call call, Response response) {
                        if (response.isSuccessful()) {
                            ResLogout resObj = (ResLogout) response.body();
                            if (resObj.message.equals("logout True")) {
                                //login start main activity
                                Intent intent = new Intent(ClassListActivity.this, MainActivity.class);
                                ClassListActivity.this.startActivity(intent);
                            } else {
                                Toast.makeText(ClassListActivity.this, "The Username or Password is incorrect!", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(ClassListActivity.this, "The Username or Password is incorrect!", Toast.LENGTH_SHORT).show();
                        }
                    }
                    @Override
                    public void onFailure(Call call, Throwable t) {
                        Toast.makeText(ClassListActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
    }
}
