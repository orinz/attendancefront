
package com.example.attendancesystem;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.remote.ApiUtils;
import com.example.remote.UserService;
import com.example.service.ResEditCheckin;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * IDE: Android Studio
 * Name package:  com.dev4u.listviewdemo
 * Name project: ListViewDemo
 * Created by Nguyen Trong Cong - NTCDE.COM
 * Date: 6/15/2016
 * Time: 10:52 PM
 */
public class StudentListAdapter extends BaseAdapter {

    public String[] idStudent;
    public String[] nameStudent;
    public int[] arrStatus;
    int passCode;
    String token_user;
    String IDClass;

    Context context;

    public StudentListAdapter(Context context, String[] idStudent, String[] nameStudent, int[] arrStatus, int passCode, String token_user, String IDClass) {
        this.context = context;
        this.idStudent = idStudent;
        this.nameStudent = nameStudent;
        this.arrStatus = arrStatus;
        this.passCode = passCode;
        this.token_user = token_user;
        this.IDClass = IDClass;
    }

    //Trả về độ dài của mảng chứa nội dung list item
    @Override
    public int getCount() {
        return idStudent.length;
    }

    //Trả về vị trí của mảng chứa nội dung list item
    @Override
    public Object getItem(int position) {
        return position;
    }

    //Trả về vị trí của mảng image list item
    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.row_student_attend, parent, false);

        TextView tvClassName = (TextView) rowView.findViewById(R.id.tvName);
        TextView tvID = (TextView) rowView.findViewById(R.id.tvMssv);
        ImageView imgAvatar = (ImageView) rowView.findViewById(R.id.imgStudent);
        ImageView imgStatus = (ImageView) rowView.findViewById(R.id.imgStatus);
        Button btnAttend = (Button) rowView.findViewById(R.id.btnAttentStudent);

        //lấy Nội dung của Item ra để thiết lập nội dung item cho đúng
        tvClassName.setText(""); //(nameStudent[position]);
        tvID.setText(idStudent[position]);
        //lấy ImageView ra để thiết lập hình ảnh cho đúng
        imgAvatar.setImageResource(R.drawable.ic_person);


        btnAttend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //check tai luc an chon diem danh cho sv thi sv da diem danh chua
                //thuc ra neu k check thi viec diem danh trung lap cung k gay anh huong den csdl
                //nen nhin nuoi viet qua



                //gv diem danh cho sv
                UserService userService = ApiUtils.getUserService(ApiUtils.BASE_URL);
                Call call = userService.editcheckin("editcheckin", passCode, token_user, IDClass, idStudent[position]);


                call.enqueue(new Callback() {
                    @Override
                    public void onResponse(Call call, Response response) {
                        if(response.isSuccessful()){
                            ResEditCheckin resEditCheckin = (ResEditCheckin) response.body();
                            if(resEditCheckin.status.equals(200)){
                                Toast.makeText(context, "Bạn đã chọn điểm danh cho sinh viên " + idStudent[position], Toast.LENGTH_SHORT).show();
                            }
                        }else{
                            Toast.makeText(context, "Thực hiện điểm danh cho sinh viên không thành công!", Toast.LENGTH_SHORT).show();
                        }
                    }
                    @Override
                    public void onFailure(Call call, Throwable t) {

                    }
                });


            }
        });
        if(arrStatus[position] == 0){
            imgStatus.setImageResource(0);
            btnAttend.setVisibility(View.INVISIBLE);
        }
        else if(arrStatus[position] == 1){//not attend
            imgStatus.setImageResource(R.drawable.ic_clear);
            btnAttend.setVisibility(View.VISIBLE);
        }
        else if(arrStatus[position] == 2){//attend
            btnAttend.setVisibility(View.INVISIBLE);
            imgStatus.setImageResource(R.drawable.ic_done_attend);
        }

        return rowView;
    }
}