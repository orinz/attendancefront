package com.example.attendancesystem;

import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.model.Classroom;
import com.example.model.Student;
import com.example.remote.ApiUtils;
import com.example.remote.UserService;
import com.example.service.ResCheckin;
import com.example.service.ResCheckout;
import com.example.service.ResListClass;
import com.example.service.ResListStudent;
import com.example.service.ResOneCheckin;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class StudentListActivity extends AppCompatActivity {
    static int numStudent = 15;
    public static int[] arrStatus = new int[numStudent];
    public static String[] rawName = new String[numStudent];
    public static String[]  rawID = new String[numStudent];
    static String status;
    static String tokencheckin;

    TextView tvClassName;
    ListView lvStudent;
    Button btnCreateAttend;
    UserService userService;
    Button btnDoneAttend;
    Button btnRefresh;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_student);
        Intent intent = getIntent();
        String nameClass = intent.getStringExtra("nameClass");

        tvClassName = (TextView) findViewById(R.id.tvClassName);
        btnCreateAttend = (Button) findViewById(R.id.btnSessionAttend);
        btnDoneAttend = (Button) findViewById(R.id.btnCheckoutAttend);
        btnRefresh = (Button) findViewById(R.id.btnRefresh);
        btnRefresh.setVisibility(View.INVISIBLE);

        tvClassName.setText("DANH SÁCH SINH VIÊN CỦA LỚP " +  nameClass);
        //get ListView theo ID từ layout xml
        lvStudent = (ListView) findViewById(R.id.lvStudent);


        String IDClass = intent.getStringExtra("nameClass");
        Integer passCode = intent.getIntExtra("passCode", 0);
        String token_user = intent.getStringExtra("token_user");
        status = intent.getStringExtra("status class");
//                Log.d("gao.tv", "status class "+status);

        StudentListAdapter lvAdapter = new StudentListAdapter(StudentListActivity.this, rawID, rawName, arrStatus, passCode, token_user, IDClass);
        lvStudent.setAdapter(lvAdapter);

        //get liststudent
        userService = ApiUtils.getUserService(ApiUtils.BASE_URL);
        Call call = userService.liststudent("liststudent", nameClass);
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                if(response.isSuccessful()){
                    ResListStudent resListStudent = (ResListStudent) response.body();
                    Student student = resListStudent.info;
                    if(resListStudent.status.equals(200)){
                        for(int i=0; i<student.IDStudent.length; i++){
//                            lvAdapter.nameStudent[i] = student.IDStudent[i]+"";
//                            lvAdapter.idStudent[i] = student.IDStudent[i]+"";
                            lvAdapter.arrStatus[i] = 0;
                        }
                        lvAdapter.idStudent = student.IDStudent;


                        lvAdapter.notifyDataSetChanged();
                    }else{
                        Toast.makeText(StudentListActivity.this, "The xxx is incorrect!", Toast.LENGTH_SHORT).show();
                    }
                }else{
                    Toast.makeText(StudentListActivity.this, "The xxx is incorrect!", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                Toast.makeText(StudentListActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });


        btnCreateAttend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Call callstatus = userService.listclass("listclass", passCode, token_user);
                callstatus.enqueue(new Callback() {
                    @Override
                    public void onResponse(Call call, Response response) {
                        if(response.isSuccessful()) {
                            ResListClass resListClass = (ResListClass) response.body();
                            if(resListClass.status.equals(200)) {
                                Classroom classroom = resListClass.info;
                                for (int i = 0; i < classroom.IDClass.length; i++) {
                                    if (classroom.IDClass[i] == Integer.parseInt(IDClass)) {
                                        status = classroom.Status[i];
                                        tokencheckin = classroom.Tokencheckin[i];
                                        break;
                                    }
                                }
                                if(status.equals("0")){
                                    Call callcheckin = userService.checkin("checkin", token_user, passCode, IDClass );
                                    callcheckin.enqueue(new Callback() {
                                        @Override
                                        public void onResponse(Call call, Response response) {
                                            if(response.isSuccessful()){
                                                ResCheckin resCheckin = (ResCheckin) response.body();
                                                if(resCheckin.status.equals(200)){
                                                    Toast.makeText(StudentListActivity.this, "Class checkin True!", Toast.LENGTH_SHORT).show();
                                                }
                                            }
                                        }
                                        @Override
                                        public void onFailure(Call call, Throwable t) {
                                        }
                                    });
                                }else{
                                    Toast.makeText(StudentListActivity.this, "Class dang diem danh", Toast.LENGTH_SHORT).show();

                                    //tao button update ket qua
                                    btnRefresh.setVisibility(View.VISIBLE);
                                    btnRefresh.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            //get arrStatus cua liststudent
                                            Call callread = userService.readone_checkin("readone_checkin", passCode, token_user, IDClass, tokencheckin);
                                            callread.enqueue(new Callback() {
                                                @Override
                                                public void onResponse(Call call, Response response) {
                                                    if(response.isSuccessful()){
                                                        ResOneCheckin resOneCheckin = (ResOneCheckin) response.body();
                                                        if(resOneCheckin.status.equals(200)){
                                                            String[] stt = resOneCheckin.readCode;

                                                            for(int i=0; i<lvAdapter.idStudent.length; i++){
                                                                lvAdapter.arrStatus[i] = 1;
                                                                for(int j=0; j<stt.length; j++){
                                                                    if(lvAdapter.idStudent[i].equals(stt[j])){
                                                                        lvAdapter.arrStatus[i] = 2;
                                                                    }
                                                                }
                                                            }

                                                            lvAdapter.notifyDataSetChanged();
                                                        }else{
                                                            Toast.makeText(StudentListActivity.this, "Token checkin khong co!", Toast.LENGTH_SHORT).show();
                                                        }
                                                    }
                                                }

                                                @Override
                                                public void onFailure(Call call, Throwable t) {

                                                }
                                            });
                                        }
                                    });
                                }
                            }
                        }
                    }
                    @Override
                    public void onFailure(Call call, Throwable t) {
                    }
                });

            }
        });

        btnDoneAttend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Call call = userService.checkout("checkout", token_user, passCode, IDClass);
                call.enqueue(new Callback() {
                    @Override
                    public void onResponse(Call call, Response response) {
                        if(response.isSuccessful()) {
                            ResCheckout resCheckout = (ResCheckout) response.body();
                            if (resCheckout.status.equals(200)) {
                                Toast.makeText(StudentListActivity.this, "Class checkout True!", Toast.LENGTH_SHORT).show();

                            //show ket qua cuoi cung cua buoi diem danh, hoac la tro ve trang thai ban dau
                                createExcelSheet(IDClass, lvAdapter.idStudent, lvAdapter.arrStatus);


                            }
                        }
                    }
                    @Override
                    public void onFailure(Call call, Throwable t) {

                    }
                });

            }
        });



    }



    //EEEEEEEEEEEEEEEEEEEEEEEEEEXCELLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL
    void createExcelSheet(String IDClass, String[] LstIDstudent, int[] results) {
//        String Fnamexls="excelSheet"+System.currentTimeMillis()+ ".xls";
        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd_HHmmss");
        Date curDate = new Date(System.currentTimeMillis());
        String str = formatter.format(curDate);

        String Fnamexls="Result"+IDClass+ ".xls";
        File sdCard = Environment.getExternalStorageDirectory();
        File directory = new File (sdCard.getAbsolutePath() + "/ExportExcel");
        directory.mkdirs();
        File file = new File(directory, Fnamexls);

        WorkbookSettings wbSettings = new WorkbookSettings();

        wbSettings.setLocale(new Locale("en", "EN"));

        WritableWorkbook workbook;
        try {
            workbook = Workbook.createWorkbook(file, wbSettings);
            int a = workbook.getNumberOfSheets();
            //workbook.createSheet("Report", 0);
            WritableSheet sheet = workbook.createSheet("Sheet "+str, a+1);
            Label label1 = new Label(0, 0, "STT");
            Label label2 = new Label(1,0,"IDSinhVien");
            Label label3 = new Label(2,0,"Results");
//            Label label4 = new Label(1,1,String.valueOf(a));
            try {
                sheet.addCell(label1);
                sheet.addCell(label2);
                sheet.addCell(label3);
                for(int i=0; i<LstIDstudent.length; i++){
                    Label label = new Label(0, i+1, String.valueOf(i+1));
                    sheet.addCell(label);
                    Label labell = new Label(1, i+1, LstIDstudent[i]);
                    sheet.addCell(labell);
                    Label labelll = new Label(2, i+1, String.valueOf(results[i]));
                    sheet.addCell(labelll);
                }
            } catch (RowsExceededException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (WriteException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            workbook.write();
            try {
                workbook.close();
            } catch (WriteException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            //createExcel(excelSheet);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

}
