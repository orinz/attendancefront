package com.example.attendancesystem;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
        import android.view.View;
        import android.view.ViewGroup;
        import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
        import android.widget.TextView;
        import android.widget.Toast;

import com.example.model.Classroom;
import com.example.remote.ApiUtils;
import com.example.remote.UserService;
import com.example.service.ResListClass;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * IDE: Android Studio
 * Name package:  com.dev4u.listviewdemo
 * Name project: ListViewDemo
 * Created by Nguyen Trong Cong - NTCDE.COM
 * Date: 6/15/2016
 * Time: 10:52 PM
 */
class ClassListAdapter extends BaseAdapter {

    public String[] result;
    public String[] status;
    Context context;
    public int[] imageId;
    int isTeacher;
    int passCode;
    String token_user;

    /**
     * Constructor này dùng để khởi tạo các giá trị
     * từ CustomListViewActivity truyền vào
     *
     * @param context  : là Activity từ CustomListView
     * @param imageId: Là danh sách image của list item truyền từ Main
     * @param result   : Danh sách nội dung của list item truyền từ Main
     */
    public ClassListAdapter(Context context, String[] result, String[] status, int[] imageId, int isTeacher, int passCode, String token_user) {
        this.context = context;
        this.result = result;
        this.status = status;
        this.imageId = imageId;
        this.isTeacher = isTeacher;
        this.passCode = passCode;
        this.token_user = token_user;
    }

    //Trả về độ dài của mảng chứa nội dung list item
    @Override
    public int getCount() {
        return result.length;
    }

    //Trả về vị trí của mảng chứa nội dung list item
    @Override
    public Object getItem(int position) {
        return position;
    }

    //Trả về vị trí của mảng image list item
    @Override
    public long getItemId(int position) {
        return position;
    }

    /**
     * hàm dùng để custom layout, ta phải override lại hàm này
     * từ CustomListViewActivity truyền vào
     *
     * @param position     : là vị trí của phần tử trong danh sách Item
     * @param convertView: convertView, dùng nó để xử lý Item
     * @param parent       : Danh sách  truyền từ Main
     * @return View: trả về chính convertView
     */

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.row_class_custom, parent, false);

        TextView tvNoiDung = (TextView) rowView.findViewById(R.id.tvContent);
        ImageView imgAvatar = (ImageView) rowView.findViewById(R.id.lvImage);
        Button btnAttend = (Button) rowView.findViewById(R.id.btnAtten);

        //lấy Nội dung của Item ra để thiết lập nội dung item cho đúng
        tvNoiDung.setText(result[position]);
        //lấy ImageView ra để thiết lập hình ảnh cho đúng
        imgAvatar.setImageResource(imageId[position]);


        if(this.isTeacher == 1){//1 is teacher
            btnAttend.setVisibility(View.INVISIBLE);
            rowView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent myIntent = new Intent(v.getContext(), StudentListActivity.class);
                    //gui ten class va status cua class ung voi vi tri click sang activity khac, o day la StudentListActivity
                    //gui ca token_user va passCode
                    myIntent.putExtra("nameClass", result[position]);
                    myIntent.putExtra("status class", status[position]);
                    myIntent.putExtra("passCode", passCode);
                    myIntent.putExtra("token_user", token_user);

                    v.getContext().startActivity(myIntent);

                }
            });
        }
        else{
            btnAttend.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub

                    //get lai status class
                    UserService userService = ApiUtils.getUserService(ApiUtils.BASE_URL);
                    Call call = userService.listclass("listclass", passCode, token_user);
                    call.enqueue(new Callback() {
                        @Override
                        public void onResponse(Call call, Response response) {
                            if(response.isSuccessful()){
                                ResListClass resListClass = (ResListClass) response.body();
                                Classroom classroom = resListClass.info;
                                for(int i=0; i<classroom.IDClass.length; i++){
                                    if(classroom.Status[position].equals("1")){

                                        Intent myIntent = new Intent(v.getContext(), AttendanceActivity.class);
                                        myIntent.putExtra("passCode", passCode);
                                        myIntent.putExtra("token_user", token_user);
                                        myIntent.putExtra("IDClass", classroom.IDClass[position]);
                                        Log.d("gao.tv", passCode+token_user+classroom.IDClass[position]);

                                        v.getContext().startActivity(myIntent);
                                        Toast.makeText(context, "Bạn đang thực hiện điểm danh cho lớp " + result[position], Toast.LENGTH_SHORT).show();
                                    }else{
                                        Toast.makeText(context, "Không có phiên điểm danh được tạo!", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            }
                        }

                        @Override
                        public void onFailure(Call call, Throwable t) {
                        }
                    });
                }
            });
        }


        //trả về View này, tức là trả luôn về các thông số mới mà ta vừa thay đổi
        return rowView;
    }
}