package com.example.attendancesystem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.Toast;

import com.example.service.ResLogin;
import com.example.model.User;
import com.example.remote.ApiUtils;
import com.example.remote.UserService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    Button btnLogin;
    EditText edtUsername;
    EditText edtPassword;
    UserService userService = null;
    Switch swIsTeacher;
    public final static int REQUEST_SETTING = 0;
    public final static int RESULT_OK = 0;
    private static final int CAMERA_PERMISSION_CODE = 100;
    private static final int STORAGE_PERMISSION_CODE = 101;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        checkPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE, STORAGE_PERMISSION_CODE);
        checkPermission(Manifest.permission.CAMERA, CAMERA_PERMISSION_CODE);

        //initView
        edtUsername = (EditText) findViewById(R.id.text_username);
        edtPassword = (EditText) findViewById(R.id.text_password);
        btnLogin = (Button) findViewById(R.id.login_button);
        swIsTeacher = (Switch) findViewById(R.id.swIsTeacher);

        //getData
        getSharePref();
        userService = ApiUtils.getUserService(ApiUtils.BASE_URL);

        //set Event
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String passCode = edtUsername.getText().toString();
                String password = edtPassword.getText().toString();
                //validate form
                if (validateLogin(passCode, password)) {
                    //do login
                    setSharePref();
                    int pass = Integer.parseInt(passCode);
                    doLogin(pass, password);
                }
            }
        });
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.login_menu, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.mySetting:
                userService = null;
                Intent itSetting = new Intent(MainActivity.this, Setting.class);
                startActivityForResult(itSetting, REQUEST_SETTING);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        userService = ApiUtils.getUserService(ApiUtils.BASE_URL);
    } //onActivityResult

    void getSharePref(){
        SharedPreferences pref = getApplicationContext().getSharedPreferences("AttendancePref", MODE_PRIVATE);
        ApiUtils.BASE_URL = pref.getString("serverLink", ApiUtils.BASE_URL);
        edtUsername.setText(pref.getString("dtUserName", ""));
        edtPassword.setText(pref.getString("password", ""));
        swIsTeacher.setChecked(pref.getBoolean("isTeacher", false));
    }
    void setSharePref(){
        SharedPreferences pref = getApplicationContext().getSharedPreferences("AttendancePref", MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean("isTeacher", swIsTeacher.isChecked());
        editor.putString("dtUserName", edtUsername.getText().toString());
        editor.putString("password", edtPassword.getText().toString());
        // Save the changes in SharedPreferences
        editor.apply(); // commit changes
    }

    private boolean validateLogin(String passCode, String password){
        if(passCode == null || passCode.trim().length() == 0){
            Toast.makeText(this, "Username is required", Toast.LENGTH_SHORT).show();
            return false;
        }
        if(password == null || password.trim().length() == 0){
            Toast.makeText(this, "Password is required", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

    }

    private void doLogin(final int passCode, final String password){

        Call call = userService.login("login", passCode, password);

        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                if(response.isSuccessful()){
                    ResLogin resObj = (ResLogin) response.body();
                    User user = resObj.info;
                    if(resObj.message.equals("login True")){
                        if (swIsTeacher.isChecked() && user.permission != 1){
                            Toast.makeText(MainActivity.this, "Username is not Teacher", Toast.LENGTH_SHORT).show();
                            return ;
                        }
                        if ( !swIsTeacher.isChecked() && user.permission != 2){
                            Toast.makeText(MainActivity.this, "Username is not Student", Toast.LENGTH_SHORT).show();
                            return ;
                        }
                        //login start main activity
                        Intent intent = new Intent(MainActivity.this, ClassListActivity.class);
                        intent.putExtra("passCode", user.passCode);
                        intent.putExtra("token_user", user.token);
                        intent.putExtra("permission", user.permission);
                        MainActivity.this.startActivity(intent);
                    } else{
                        Toast.makeText(MainActivity.this, "The Username or Password is incorrect!", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(MainActivity.this, "The Username or Password is incorrect!", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                Toast.makeText(MainActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    void checkPermission(String Permission, int requestCode) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this,  new String[]{Permission },  requestCode );
            }
    }
}