package com.example.remote;

public class ApiUtils {
    public static String BASE_URL = "http://ec2-54-254-15-134.ap-southeast-1.compute.amazonaws.com:8092/";

    public static UserService getUserService(String my_url){
        return RetrofitClient.getUser(my_url).create(UserService.class);
    }
}
