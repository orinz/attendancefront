package com.example.remote;

import com.example.service.ResAttend;
import com.example.service.ResCheckin;
import com.example.service.ResCheckout;
import com.example.service.ResEditCheckin;
import com.example.service.ResListClass;
import com.example.service.ResListStudent;
import com.example.service.ResLogin;
import com.example.service.ResLogout;
import com.example.service.ResOneCheckin;

import java.io.File;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface UserService {
    @POST("/api/face/v1/user")
    Call<ResLogin> login(
            @Query("action") String action,
            @Query("passCode") int passCode,
            @Query("password") String Password
    );
    @POST("/api/face/v1/user")
    Call<ResLogout> logout(
            @Query("action") String action,
            @Query("passCode") int passCode,
            @Query("token_user") String token_user
    );

    @POST("/api/face/v1/checkclass")
    Call<ResListClass> listclass(
            @Query("action") String action,
            @Query("passCode") int passCode,
            @Query("token_user") String token_user
    );

    @POST("/api/face/v1/checkclass")
    Call<ResListStudent> liststudent(
            @Query("action") String action,
            @Query("IDClass") String IDClass
    );

    @POST("api/face/v1/classcheckin")
    Call<ResCheckin> checkin(
            @Query("action") String action,
            @Query("token_user") String token_user,
            @Query("passCode") int passCode,
            @Query("IDClass") String IDClass
    );

    @POST("api/face/v1/classcheckin")
    Call<ResCheckout> checkout(
            @Query("action") String action,
            @Query("token_user") String token_user,
            @Query("passCode") int passCode,
            @Query("IDClass") String IDClass
    );

    @Multipart
    @POST("api/face/v1/detectgetembedding")
    Call<ResAttend> search(
            @Query("action") String action,
            @Query("token_user") String token_user,
            @Query("passCode") int passCode,
            @Query("IDClass") String IDClass,
            @Part MultipartBody.Part file
    );

    @POST("api/face/v1/readcheckin")
    Call<ResOneCheckin> readone_checkin(
            @Query("action") String action,
            @Query("passCode") int passCode,
            @Query("token_user") String token_user,
            @Query("IDClass") String IDClass,
            @Query("token_checkin") String token_checkin
    );

    @POST("api/face/v1/classcheckin")
    Call<ResEditCheckin> editcheckin(
            @Query("action") String action,
            @Query("passCode") int passCode,
            @Query("token_user") String token_user,
            @Query("IDClass") String IDClass,
            @Query("IDstudent") String IDstudent
    );
}
