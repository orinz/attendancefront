package com.example.RetinaFace;

import android.content.res.AssetManager;
import android.graphics.Bitmap;

public class RetinaFace {

    static {
        System.loadLibrary("RetinaFace");
    }
    public native boolean Init(AssetManager mgr);
    public native float[] Detect(Bitmap bitmap);
}
